/*************************************************
* Arquivo com funcoes auxiliares para o programa *
* de simulacao de MD do argonio					 *
*************************************************/
#include <stdlib.h>
#include <math.h>
#include <stdio.h> // apagar apos debug
#include "aux.h"
#include "config.h"

void vel(double v[N][DIM], double T){
	double vCM[3] = {0, 0, 0};
    for(int n = 0; n < N; n++){
    	for(int i = 0; i < 3; i++){
        	vCM[i] += v[n][i];
        } 
    }
    for(int i = 0; i < 3; i++){
    	vCM[i] /= N;            // Calculo da velocidade do centro de massa
    }
    for(int n = 0; n < N; n++){
   		for(int i = 0; i < 3; i++){
        	v[n][i] -= vCM[i];  // Mudar as velocidades para que o centro de massa fique parado
        }
    }
    reescVel(v, T);
}

double temp(double v[N][DIM]){
	double sum = 0;
    for(int i = 0; i < N; i++){
        for(int k = 0; k < DIM; k++){
            sum += v[i][k] * v[i][k];
        }   
    }   
     return (sum / ( 3 * ( N - 1)));
}   
    

double GaussMaxwell(){ //https://phoxis.org/2013/05/04/generating-random-numbers-from-normal-distribution-in-c/
	unsigned short int mu = 0;	// Media 0
	unsigned short int sigma = 1;// Variancia unitaria
	double U1, U2, W, mult;
	static double X1, X2;

	do{
		U1 = -1 + ((double) rand() / RAND_MAX) * 2;
		U2 = -1 + ((double) rand() / RAND_MAX) * 2;
		W = pow(U1, 2) + pow(U2, 2);
	}while(W >= 1 || W == 0);
	mult = sqrt((-2*log(W))/W);
	X1 = U1 * mult;
	X2 = U2 * mult;

	return ((double)mu + (double)sigma * (double) X1);
}

void reescVel( double v[N][DIM], double T){
	/*	Funcao para a reescala da velocidade
	   	para velocidade regulada de acordo com
		a temperatura */

	double vSgdSum = 0;
	for(int n = 0; n < N; n++){
		for(int i = 0; i < 3; i++){
			vSgdSum += v[n][i] * v[n][i];
		}
	}
	double lambda = sqrt( 3 * (N - 1) * T / vSgdSum);
	for(int n = 0; n < N; n++){
		for(int i = 0; i < 3; i++){
			v[n][i] *= lambda;
		}
	}
}

void calcAcc(double acc[N][DIM], double r[N][DIM], double a, double L){
	for(int i = 0; i < N; i++){
		for(int k = 0; k < 3; k++){
			acc[i][k] = 0;
		}
	}

	for (int i = 0; i < N-1; i++)        // loop over all distinct pairs i,j
    	for (int j = i+1; j < N; j++) {
        	double rij[3];               // position of i relative to j
            double rSqd = 0;
            for (int k = 0; k < 3; k++) {
                rij[k] = r[i][k] - r[j][k];
                // closest image convention
            if (fabs(rij[k]) > 0.5 * L) {
                 if (rij[k] > 0)
                    rij[k] -= L;
                 else
            		rij[k] += L;
            }
        	rSqd += rij[k] * rij[k];
        }
        double f = 24 * (2 * pow(rSqd, -7) - pow(rSqd, -4));
        for (int k = 0; k < 3; k++) {
            acc[i][k] += rij[k] * f;
        	acc[j][k] -= rij[k] * f;
		}
	}

}

void VERLET(double L, double dt, double acc[N][DIM], double r[N][DIM], double v[N][DIM], double a){
	/* Metodo de integracao da equacao de movimento */
	printf("%f\t%f\n",v[0][0], r[0][0]);
	calcAcc(acc, r, a, L);
	for(int i = 0; i < N; i++){
		for(int k = 0; k < DIM; k++){
			r[i][k] += v[i][k] * dt + 0.5 * acc[i][k] * dt * dt;

			// Uso das condicoes de contorno periodicas
			if(r[i][k] < 0){
				r[i][k] += L;
			}
			if(r[i][k] >= L){
				r[i][k] -= L;
			}
			v[i][k] += 0.5 * acc[i][k] * dt;
		}
	}
	calcAcc(acc, r, a, L);
	for(int i = 0; i < N; i++){
		for(int k = 0; k < DIM; k++){
			v[i][k] += 0.5 * acc[i][k] * dt;
		}
	}
	printf("%f\t%f\n\n\n",v[0][0], r[0][0]);
}


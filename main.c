#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "aux.h"
#include "config.h"

int main(){
	double r[N][DIM];				// Array das posicoes
	double v[N][DIM];				// Array das velocidades
	double ac[N][DIM];				// Array das aceleracoes

	/*	Inicio do calculo das propriedades da rede ( FCC )	*/
	double L = pow(N / RHO, 1.0/(double) DIM);	// Tamanho do lado da celula unitaria
	int M = 1;									// Achar M suficientemente grande para
	while( 4 * M * M * M < N){					// acomodar N atomos em uma rede FCC
		++M;
	}
	double a = L / M;							// Constade da rede de uma celula convencional
	double celulaX[4] = { 0.25, 0.75, 0.75, 0.25};// Coordenadas x dos vertices da celula unitaria
	double celulaY[4] = { 0.25, 0.75, 0.25, 0.75};// Coordenadas y dos vertices da celula unitaria
	double celulaZ[4] = { 0.25, 0.25, 0.75, 0.75};// Coordenadas z dos vertices da celula unitaria
    /********************************************************/

	/*	Alocacao dos atomos na rede	*/
	int n = 0;						// Numero de atomos posiconados ate o momento
	for(int x = 0; x < M; x++){
		for(int y = 0; y < M; y++){
			for(int z = 0; z < M; z++){
				for(int k = 0; k < 4; k++){
					if(n < N){
						r[n][0] = ( x + celulaX[k] ) * a;
						r[n][1] = ( y + celulaY[k] ) * a;
						r[n][2] = ( z + celulaZ[k] ) * a;
						++n;
					}
				}
			}
		}
	}
	/********************************/

	/* Inicializacao das velocidades	*/
	double T = T_I;
	for(int n = 0; n < N; n++){
		for(int i = 0; i < 3; i++){
			v[n][i] = GaussMaxwell();
		}
	}
	vel(v, T);
	/*********************************/
	double dt = 0.0001;
	for(T = T_I; T < T_F; T += 0.1){
		double t_cal = 0;
		for(int i = 0; i < N; i++){
			for(int k = 0; k < 3; k++){
				t_cal += v[i][k] * v[i][k];
			}
		}
		t_cal /= (3 * ( N - 1));
//		printf("%f\n",v[0][0]); //, %f\n", t_cal, T);
		for(int i = 0; i < 1000; i++){
			VERLET(L, dt, acc[N][DIM], r[N][DIM], v[N][DIM], a);
		}			
	}		
}


#include "config.h"

double GaussMaxwell();
void reescVel( double v[N][DIM], double T);
void calcVel(double acc[N][DIM], double r[N][DIM], double a, double L);
void VERLET(double L, double dt, double acc[N][DIM], double r[N][DIM], double v[N][DIM], double a);
void vel(double v[N][DIM], double T);
double temp(double v[N][DIM]);

